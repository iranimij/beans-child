<?PHP

/**
 * Initialize Beans theme framework.
 *
 */

function mytheme_kirki_sections($wp_customize)
{

    $wp_customize->add_section('change_layouts', array(
        'title' => __("Change Layout", "my-theme"),
        'priority' => 10,
    ));

}

add_action('customize_register', 'mytheme_kirki_sections');


function mytheme_kirki_fields($fields)
{

    $fields[] = array(
        'type' => 'select',
        'settings' => 'change_layouts',
        'label' => __('Change sidebar Position', 'my-theme'),
        'section' => 'change_layouts',
        'choices' => array(
            '' => __("select", "my-theme"),
            'full' => __("Full Width", "my-theme"),
            'left' => __("Left", "my-theme"),
            'right' => __("Right", "my-theme")
        ),
        'priority' => 10,
    );

    return $fields;

}

add_filter('kirki/fields', 'mytheme_kirki_fields');

function sidebar_force_layout($layout)
{

    $sidebar_position = get_theme_mod("change_layouts", "right");
    $result = $layout;
    switch ($sidebar_position) {
        case "left":
            $result = "sp_c";
            break;
        case "right":
            $result = "c_sp";
            break;
        case "full":
            $result = "c";
            break;
    }
    return $result;

}

add_action("wp_head", function () {
    if (is_page(get_the_ID())) {
        add_filter('beans_layout', 'sidebar_force_layout', 1, 1);
    }
});


function wpdocs_child_theme_setup()
{
    load_child_theme_textdomain('my-theme', get_stylesheet_directory() . '/languages');
}

add_action('after_setup_theme', 'wpdocs_child_theme_setup');





